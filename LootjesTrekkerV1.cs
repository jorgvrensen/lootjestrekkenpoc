﻿using System;
using System.Linq;

namespace LootjesTrekkenPoc
{
    public class LootjesTrekkerV1
    {
        public static int resetCounter { get; set; }
        public static int lootjesCounter { get; set; }

        private readonly int _aantalDeelnemers;
        private readonly string[] _namen;
        private readonly Random random = new Random();

        public LootjesTrekkerV1(int aantalDeelnemers, string[] namen)
        {
            _aantalDeelnemers = aantalDeelnemers;
            _namen = namen;
        }

        public string[] TrekLootjes()
        {
            var bakMetLootjes = _namen.ToList().GetRange(0, _aantalDeelnemers);
            var trekkingsResultaat = new string[_aantalDeelnemers];

            // Elke deelnemer trekt een lootje
            for (var deelnemer = 0; deelnemer < _aantalDeelnemers; deelnemer++)
            {
                var geldigeTrekking = false;

                do
                {
                    // als dit het laatste lootje is, en dat is toevallig die van de huidige (laatste) deelnemer:
                    if (bakMetLootjes.Count == 1 && bakMetLootjes.First().Equals(_namen[deelnemer]))
                    {
                        //reset alles - hele trekking opnieuw
                        deelnemer = 0;
                        bakMetLootjes = _namen.ToList().GetRange(0, _aantalDeelnemers);
                        resetCounter++;
                    }

                    //trek lootje
                    var getrokkenLootje = bakMetLootjes[random.Next(bakMetLootjes.Count)];
                    lootjesCounter++;

                    //controleer lootje: als niet zichzelf dan geldig
                    if (getrokkenLootje != _namen[deelnemer])
                    {
                        trekkingsResultaat[deelnemer] = getrokkenLootje;
                        bakMetLootjes.Remove(getrokkenLootje);
                        geldigeTrekking = true;
                    }

                } while (!geldigeTrekking); 
            }
            return trekkingsResultaat;
        }
    }
}
