﻿using System;

namespace LootjesTrekkenPoc
{
    public class Program
    {
        private const int AantalDeelnemers = 5; //max 20
        private const int AantalIteraties = 5;
        private const bool PrintElkeTrekking = true; //true geeft complete trekkingsuitslag

        private static readonly string[] Namen = new string[]
        {
            "Jan", "Piet", "Klaas", "Henk", "Joop", "Ad", "Bart", "Peter", "Mark", "Sjaak",
            "Jet", "Miep", "Karin", "Anna", "Joke", "An", "Jana", "Lotte", "Inge", "Elske"
        };

        public static void Main()
        {
            var lootjesTrekker = new LootjesTrekkerV1(AantalDeelnemers, Namen);

            for (var iteratie = 1; iteratie < AantalIteraties + 1; iteratie++)
            {
                var resultaat = lootjesTrekker.TrekLootjes();

                if (PrintElkeTrekking) PrintTrekkingsUitslag(resultaat, iteratie);
            }

            PrintStats();
        }

        private static void PrintTrekkingsUitslag(string[] resultaat, int trekking)
        {
            Console.WriteLine($"=======< Trekking {trekking} >=======");
            for (var i = 0; i < AantalDeelnemers; i++)
            {
                Console.WriteLine($"- {Namen[i]} heeft {resultaat[i]} getrokken");
            }
            Console.WriteLine();
        }

        private static void PrintStats()
        {
            var hertrekkingsPercentage = (double) LootjesTrekkerV1.resetCounter / AantalIteraties * 100;

            Console.WriteLine($"Aantal deelnemers: {AantalDeelnemers}" +
                $"\nAantal trekkingen: {AantalIteraties}" +
                $"\nAantal hertrekkingen: {LootjesTrekkerV1.resetCounter}" +
                $"\nTotaal aantal getrokken lootjes: {LootjesTrekkerV1.lootjesCounter}" +
                "\nHertrekkingen percentage: {0:0.00}%", hertrekkingsPercentage);
        }
    }
}
